#include <string.h>
#include <dlfcn.h>
#include <stdio.h>
#include <android/log.h>

static int FileExists(const char *filename) {
  FILE *fp = fopen(filename, "r");
  if (fp != NULL)
    fclose(fp);
  return (fp != NULL);
}

void *DLOPEN(const char *filename, int flag) {
  __android_log_print(ANDROID_LOG_INFO, "dlopen-hack", "dlopen(%s, %d) called",
                      filename, flag);
  char buff[1024] = {0};
  snprintf(buff, sizeof(buff), "%s/%s", "/system/lib", filename);
  if (FileExists(buff)) {
    __android_log_print(ANDROID_LOG_INFO, "dlopen-hack", " --> dlopen(%s, %d)",
                        buff, flag);
    return dlopen(buff, flag);
  }
  snprintf(buff, sizeof(buff), "%s/%s", "/vendor/lib", filename);
  if (FileExists(buff)) {
    __android_log_print(ANDROID_LOG_INFO, "dlopen-hack", " --> dlopen(%s, %d)",
                        buff, flag);
    return dlopen(buff, flag);
  }
  __android_log_print(ANDROID_LOG_INFO, "dlopen-hack", " --> dlopen(%s, %d)",
                      filename, flag);
  return dlopen(filename, flag);
}
