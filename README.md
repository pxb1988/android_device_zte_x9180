# Device configuration for the ZTE V5 RedBull (X9180)

Local manifest for CM13

.repo/local_manifests/roomservice.xml
```xml
<?xml version="1.0" encoding="UTF-8"?>
<manifest>
  <remote fetch="https://bitbucket.org" name="bit" revision="x9180-bob-cm-13.0" />
  <project name="pxb1988/android_kernel_zte_x9180.git" path="kernel/ZTE/X9180" remote="bit" />
  <project name="pxb1988/android_device_zte_x9180.git" path="device/ZTE/X9180" remote="bit" />
  <project name="pxb1988/android_vendor_zte_x9180.git" path="vendor/ZTE/X9180" remote="bit" />
  <project name="CyanogenMod/android_device_qcom_common" path="device/qcom/common" remote="github" />
  <project name="CyanogenMod/android_external_stlport" path="external/stlport" remote="github" />
  <project name="CyanogenMod/android_external_sony_boringssl-compat" path="external/sony/boringssl-compat" remote="github" />
</manifest>
```

## SELINUX patch
the selinux is fail on default build, so i have to mark selinux as permissive by adding `androidboot.selinux=permissive` to kernel cmdline
but system/vold is failing without a selinux context, so the following patch must apply to system/vold before build CM-13

```bash
pushd system/vold
git am ../../device/ZTE/X9180/repack-boot-img/system_vold_do_not_abort_on_setexeccon_failure.patch
popd
```

## ril patch

```bash
pushd hardware/ril-caf
git am ../../device/ZTE/X9180/repack-boot-img/ril-caf-fix-crash-on-rild.patch
popd
```

## Repackage boot.img
after build the CM-13, we also need to patch the boot.img before flash it to device

```bash
./device/ZTE/X9180/repack-boot-img/repack.sh
```
